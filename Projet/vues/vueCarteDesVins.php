<?php $titre = ""; ?>

<?php ob_start(); ?>


<?php
#code PHP qui permet l'ajout dans un fichier
$message = '';
$error = '';
if(isset($_POST["submit"]))
{
     if(empty($_POST["vin"]))
     {
          $error = "Enter le nom d'un vin";
     }
     else if (empty($_POST["met"]))
     {
          $error = "Enter le nom d'un met";
     }
     else if (empty($_POST["contributeur"]))
     {
          $error = "Enter votre pseudo";
     }
     else
     {
          if(file_exists('Accords_mets_vins_suggestion.json'))
          {
               $current_data = file_get_contents('Accords_mets_vins_suggestion.json');
               $array_data = json_decode($current_data, true);
               $extra = array(
                    'vin'               =>     $_POST['vin'],
                    'met'               =>     $_POST['met'],
                    'couleur'           =>     $_POST['couleur'],
                    'categorie'         =>     $_POST['categorie'],
                    'temperature'       =>     $_POST['temperature'],
                    'contributeur'      =>     $_POST['contributeur']
               );
               $array_data[] = $extra;
               $final_data = json_encode($array_data);
               if(file_put_contents('Accords_mets_vins_suggestion.json', $final_data))
               {
                    $message = "Suggestion Ajouté";
               }
          }
          else
          {
               $error = 'JSON File not exits';
          }
     }
}

?>
<!--importation de librairie ajax et boostrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<!--importation de fichier json-->
<script src="Accords_mets_vins.json" ></script>
<script src="Accords_mets_vins_suggestion.json" ></script>

<div class="contenu">
        <br>
        <div class="msgacc">
                <h1> Les accords mets et vins </h1>
                <br>
                <h4> Idées d'entrées, plats et desserts pour accompagner votre vin et inversement ! </h4>
        </div>
        <br>
        <div class="container justify-content-between">
              <img  class="img1 col-md-11" src="image/Accords-mets-vins.png" id="image">
              <br><br><br>

              <button type=button class="btn btn-primary" id="btn-crea" data-toggle="modal" data-target="#modal-upload-accord"><b>Proposer un accord met-vin</b></button>
              <br><br>
              <a href="index.php?page=accords_mets_vins"<u>Voir les accords proposés par les autres utilisateurs</u></a>
              <br><br>
        </div>


        <div class="container justify-content-between">
                <h4 class="rubrique-info"><b> Je cherche un met pour accompagner mon vin, je sélectionne sa couleur : </b>
                <i class="fa fa-hand-o-down fa-2x" aria-hidden="true" style="color:white"></i></h4>
                <br><br><br>
                <div class="row">
                        <section class="Sgauche col-sm-3" id="Vin_Rouge"  onclick="Affiche_liste_vins('Vin_Rouge')">
                                <h4> Vin rouge </h4>
                        </section>
                        <div class="col-sm-1">
                        </div>
                        <section class="milieuD col-sm-3" id="Tous_les_vins" onclick="Affiche_liste_vins('Tous_les_vins')">
                                <h4> Tous les vins </h4>
                        </section>
                        <div class="col-sm-1">
                        </div>
                        <section class="Sdroite col-sm-3 " id="Vin_Blanc" onclick="Affiche_liste_vins('Vin_Blanc')">
                                <h4> Vin blanc </h4>
                        </section>
                </div>
        </div>
        <br><br><br>
        <div class="container justify-content-between">
                <div class="row">
                        <div class="col-sm-6">
                                <div class="liste-vin" id="vin">
                                </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="liste-vin" id="info_vin">
                                </div>
                        </div>
                </div>
                <br><br>
        </div>
        <div class="container justify-content-between">
                <h4 class="rubrique-info"><b> Je cherche un vin pour accompagner mon met, je selectionne sa catégorie : </b><i class="fa fa-hand-o-down fa-2x" aria-hidden="true" style="color:white"></i></h4>
                <br><br><br>
                <div class="row">
                        <section class="Sgauche col-sm-3" id="entrée" onclick="Affiche_liste_mets('entrée')">
                                <h4> Entrées </h4>
                        </section>
                        <div class="col-sm-1">
                        </div>
                        <section class="milieuG col-sm-3" id="plat" onclick="Affiche_liste_mets('plat')">
                                <h4> Plats </h4>
                        </section>
                        <div class="col-sm-1">
                        </div>
                        <section class="Sdroite col-sm-3 " id="dessert" onclick="Affiche_liste_mets('dessert')">
                                <h4> Desserts </h4>
                        </section>
                </div>
                <br><br>
        </div>
        <br><br>
        <div class="container justify-content-between">
                <div class="row">
                        <div class="col-sm-6">
                                <div class="liste-vin" id="met">
                                </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="liste-vin" id="info_met">
                                </div>
                        </div>
                </div>
        </div>
        <br><br>
        <div class="container justify-content-between">
                <div class="row">
                        <div class="col-sm-4">
                        </div>
                        <section class="Tmilieu col-sm-3" id="entrée" onclick="Affiche_selectbtn()">
                                <h4> Tous les mets</h4>
                        </section>
                        <div class="col-sm-4">
                        </div>
                </div>
        </div>
        <br><br>
        <div class="container justify-content-between">
                <div id="select"></div>
                <div id="information" class="liste-vin"></div>
        </div>
        <br><br>
</div>






<!-- Modal ajout de sujet -->
        <div id="modal-upload-accord" class="modal fade" role="dialog" style="display: none;">
                <div class="modal-dialog">
                        <!-- Modal content-->
                        <form method="post" class="modal-content" id="upload-form" enctype="multipart/form-data">
                                <div class="modal-header">
                                        <button type="button"  id="end"class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title">Ajout accord-met-vin</h4>
                                </div>
                                <div class="modal-body">
                                        <div class="form-group">
                                                <br>
                                                <label>Pseudo utilisateur</label>
                                                <input type="text" name="contributeur" class="form-control" /><br>
                                        </div>
                                        <div class="form-group">
                                                <label>Nom vin</label>
                                                <input type="text" name="vin" class="form-control" /><br>
                                        </div>
                                        <div class="form-group">
                                                <label>Nom met</label>
                                                <input type="text" name="met" class="form-control" /><br>
                                        </div>
                                        <div class="form-group">
                                                <label>Couleur</label>
                                                <select class="form-control" id="upload-couleur" name="couleur" type="text">
                                                        <option id="val1" value="Rouge">Rouge</option>
                                                        <option id="val2" value="Blanc">Blanc</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                                <label>Catégorie</label>
                                                <select class="form-control" id="upload-couleur" name="categorie" type="text">
                                                        <option id="val1" value="Entrée">Entrée</option>
                                                        <option id="val2" value="Plat">Plat</option>
                                                        <option id="val3" value="Dessert">Dessert</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                                <label>Température</label>
                                                <select class="form-control" id="upload-couleur" name="temperature" type="text">
                                                        <option id="val1" value="Chaud">Chaud</option>
                                                        <option id="val2" value="Froid">Froid</option>
                                                </select>
                                        </div>
                                        <div class="modal-footer">
                                                <input type="submit" name="submit" value="Soumettre" class="btn btn-info" /><br>
                                        </div>
                                </div>
                        </form>
                </div>
        </div>
        
        <?php
            if($error!="")
            {
                echo "<script>alert(\"$error\")</script>";
            }
        ?>
        
        <?php
            if($message!="")
            {
                echo "<script>alert(\"$message\")</script>";
            }
        ?>

<!-- Début js -->
<script>

// récupère le modal
var modal = document.getElementById("modal-upload-accord");

// récupère le bouton pour créée le modal
var btn = document.getElementById("btn-crea");

// récupère le <span> element pour fermer le modal
var end = document.getElementById("end");

// Quand l'utilisateur click sur le bouton , ouvre le modal
btn.onclick = function() {
  modal.style.display = "block";
}

// Quand l'utilisateur click sur le <span> (x), ferme le modal
end.onclick = function() {
  modal.style.display = "none";
}

//Quand l'utilisateur click n'importe ou hors du modal, ferme le modal
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

let afficher=Array();
let afficher2=Array();


// Un tableau qui va contenir toutes tes images.
           var images = new Array();
           images.push("image/Accords-mets-vins.png");
           images.push("image/accords-mets-vins-2.jpg");

           var pointeur = 0;

           // fonction pour changer l'image d'accueil tout les 2sec
           function ChangerImage(){
                   document.getElementById("image").src = images[pointeur];

                   if(pointeur < images.length-1){
                           pointeur++;
                   }
                   else{
                           pointeur = 0;
                   }
                   window.setTimeout("ChangerImage()", 3000)
           }

           // Charge la fonction
           window.onload = function(){
           ChangerImage();
           }

   console.log(DATA);
   console.log(DATA[0].Nom_vin);
   console.log(DATA.length);


   //fonction qui va afficher la liste des vins du fichier Accords_mets_vins.json
   function Affiche_liste_vins(id)
   {
        let cpt=0;
        let tab =Array();
        tab[0]= titre_res();
        clear_todo_vin();
        if (id=="Tous_les_vins")
        {
            List=ListV.filter(getUniqueVal);
            newList=Liste_croissante(List);
            for(let i=0; i<newList.length;i++)
            {
                tab[i+1]=`<div class="liste" id="infos_vin${i}" >${newList[i]}</div>`;
                cpt=cpt+1;
            }
            console.log(cpt);
        }
        else if(id=="Vin_Rouge")
        {
            List=ListVR.filter(getUniqueVal);
            newList=Liste_croissante(List);
            for(let i=0; i<newList.length;i++)
            {
                tab[i+1]=`<div class="liste" id="infos_vin${i}" >${newList[i]}</div>`;
                cpt=cpt+1;
            }
            console.log(cpt);
        }
        else if(id=="Vin_Blanc")
        {
            List=ListVB.filter(getUniqueVal);
            newList=Liste_croissante(List);
            for(let i=0; i<newList.length;i++)
            {
                tab[i+1]=`<div class="liste" id="infos_vin${i}" >${newList[i]}</div>`;
                cpt=cpt+1;
            }
            console.log(cpt);
        }
        document.getElementById('vin').innerHTML=tab.join("");
        for(let i=0; i<newList.length;i++)
            affiche_todo_vin(i);
   }


   //fonction qui sera utiliser pour l'affichage sur le site pour les accord proposé
   function titre_res()
   {
        return `<h5 style="color:white"> Voici les résultats,<br> cliquez pour découvrir l'accord proposé : </h5><br>` ;
   }

   
   //function appeler dans onclick du boutton 'tout les mets' qui affichera un select avec les option
   function Affiche_selectbtn()
   {
        document.getElementById('select').innerHTML= `<select id="selects" onclick="Afficher_selection()">
                <option id='val1' value='val1'>Tout les mets</option>
                <option id='val2' value='val2'>Sucré</option>
                <option id='val3' value='val3'>Salé</option>
                <option id='val4' value='val4'>Chaud</option>
                <option id='val5' value='val5'>Froid</option>
                </select>`;
   }

   
   //fonction qui selon la valeur du select renvoie les mets désiré  par example les mets 
   function Afficher_selection()
   {
        if(document.getElementById('selects').value=="val1")
        {
            let List=ListMet.filter(getUniqueVal);
            let newList = Liste_croissante(List);
            let tab=Array();
            for(let i=0;i<newList.length;i++)
            {
                tab[i]=`<div class="liste">${newList[i]}</div>`;
            }
            document.getElementById('information').innerHTML=tab.join("");
        }

        if(document.getElementById('selects').value=="val2")
        {
            let List=ListMetSu.filter(getUniqueVal);
            let newList = Liste_croissante(List);
            let tab=Array();
            for(let i=0;i<newList.length;i++)
            {
                tab[i]=`<div class="liste">${newList[i]}</div>`;
            }
            document.getElementById('information').innerHTML=tab.join("");
        }

        if(document.getElementById('selects').value=="val3")
        {
            let List=ListMetSa.filter(getUniqueVal);
            let newList = Liste_croissante(List);
            let tab=Array();
            for(let i=0;i<newList.length;i++)
            {
                tab[i]=`<div class="liste">${newList[i]}</div>`;
            }
            document.getElementById('information').innerHTML=tab.join("");
        }

        if(document.getElementById('selects').value=="val4")
        {
            let List=ListMetCh.filter(getUniqueVal);
            let newList = Liste_croissante(List);
            let tab=Array();
            for(let i=0;i<newList.length;i++)
            {
                tab[i]=`<div class="liste">${newList[i]}</div>`;
            }
            document.getElementById('information').innerHTML=tab.join("");
        }

        if(document.getElementById('selects').value=="val5")
        {
            let List=ListMetFr.filter(getUniqueVal);
            let newList = Liste_croissante(List);
            let tab=Array();
            for(let i=0;i<newList.length;i++)
            {
                tab[i]=`<div class="liste">${newList[i]}</div>`;
            }
            document.getElementById('information').innerHTML=tab.join("");
        }
   }

   //fonction qui va afficher la liste des mets du fichier Accords_mets_vins.json
   function Affiche_liste_mets(id)
   {
        var newList=Array();
        var cat;
        let cpt=0;
        let tab =Array();
        tab[0]= titre_res();
        clear_todo_met();
        if (id=="entrée")
        {
            cat="Entrée";
            List=ListE.filter(getUniqueVal);
            newList=Liste_croissante(List);

            for(let i=0; i<newList.length;i++)
            {
                tab[i+2]=`<div class="liste" id="infos_met${i}" >${newList[i]}</div>`;
                cpt=cpt+1;
            }  
            console.log(cpt);
        }
        else if(id=="plat")
        {
            cat="Plat";
            List=ListP.filter(getUniqueVal);
            newList=Liste_croissante(List);
            for(let i=0; i<newList.length;i++)
            {
                tab[i+2]=`<div class="liste" id="infos_met${i}" >${newList[i]}</div>`;
                cpt=cpt+1;
            }  
            console.log(cpt);
        }
        else if(id=="dessert")
        {
            cat="Dessert";
            List=ListD.filter(getUniqueVal);
            newList=Liste_croissante(List);
            for(let i=0; i<newList.length;i++)
            {
                tab[i+2]=`<div class="liste" id="infos_met${i}" >${newList[i]}</div>`;
                cpt=cpt+1;
            } 
            console.log(cpt);
        }
        document.getElementById('met').innerHTML=tab.join("");
        for(let i=0; i<newList.length;i++)
            affiche_todo_met(i);
   }

   function getUniqueVal(value, index, self) 
   {
        return self.indexOf(value) === index;
   }

   // Création liste des desserts qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListD=[];
   for(let i=0; i<DATA.length;i++)
   {
           if(DATA[i].Catégorie=="Dessert")
           {
              ListD.push(DATA[i].Nom_met);
           }
   }

   // Création liste des plats qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListP=[];
   for(let i=0; i<DATA.length;i++)
   {
          if(DATA[i].Catégorie=="Plat")
          {
             ListP.push(DATA[i].Nom_met);
          }
   } 

   // Création liste des entrées qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListE=[];
   for(let i=0; i<DATA.length;i++)
   {
           if(DATA[i].Catégorie=="Entrée")
           {
               ListE.push(DATA[i].Nom_met);
           }
   }

   // Création liste des vins qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListV=[];
   for(let i=0; i<DATA.length;i++)
   {
           ListV.push(DATA[i].Nom_vin);
   }

   // Création liste des vins rouges qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListVR=[];
   for(let i=0; i<DATA.length;i++)
   {
           if(DATA[i].Couleur_vin=="Rouge")
           {
               ListVR.push(DATA[i].Nom_vin);
           }
   }

   // Création liste des vins blancs qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListVB=[];
   for(let i=0; i<DATA.length;i++)
   {
           if(DATA[i].Couleur_vin=="Blanc")
           {
               ListVB.push(DATA[i].Nom_vin);
           }
   }

   // Création liste de tout les mets qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListMet=[];
   for(let i=0; i<DATA.length;i++)
   {
          ListMet.push(DATA[i].Nom_met);
   }

   // Création liste des mets sucré qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListMetSu=[];
   for(let i=0; i<DATA.length;i++)
   {
           if(DATA[i].Gout=="Sucré")
                   ListMetSu.push(DATA[i].Nom_met);
   }

   // Création liste des mets salé qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListMetSa=[];
   for(let i=0; i<DATA.length;i++)
   {
           if(DATA[i].Gout=="Salé")
                   ListMetSa.push(DATA[i].Nom_met);
   }

   // Création liste des mets chaud qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListMetCh=[];
   for(let i=0; i<DATA.length;i++)
   {
           if(DATA[i].Temperature=="Chaud")
                   ListMetCh.push(DATA[i].Nom_met);
   }

   // Création liste des mets froid qu'on va filtrer avec getUniqueVal pour supprimer doublon
   var ListMetFr=[];
   for(let i=0; i<DATA.length;i++)
   {
           if(DATA[i].Temperature=="Froid")
                   ListMetFr.push(DATA[i].Nom_met);
   }

   // Trie des listes par odre alphabétique
   function Liste_croissante(liste) 
   {
       const res = Array.from(liste);
       res.sort((n1, n2) => n1 > n2? 1 : n1 < n2? -1 : 0);
       console.log(res);
       return res;
   }

   //fonction qui va gérer l'affichage des mets associer au vin choisie dans la liste
   function affiche_todo_vin(i)
   {
           let j=0;
           document.getElementById("infos_vin"+i).addEventListener("click",function(){
                   clear_todo_vin();
                   let nom = document.getElementById("infos_vin"+i).textContent;
                   console.log(nom);
                   afficher[0]=`<h5 style="color:white"> Mets suggérés : </h5><br>`;
                   for(let x=0;x<DATA.length;x++)
                   {
                           if(DATA[x].Nom_vin==nom)
                           {
                                   afficher[j+1]=`<div class="liste ">${DATA[x].Nom_met}</div>`;
                                   afficher[j+1]+=`<img src="${DATA[x].Image}" style="height:100px" ; >`;
                                   j++;
                                   console.log(DATA[x].Nom_met);
                           }
                   }
                   console.log(afficher)
                   document.getElementById("info_vin").innerHTML =afficher.join("");
           });
   }

   //fonction qui va gérer l'affichage des vins associer au mets choisie dans la liste
   function affiche_todo_met(i)
   {
           let j=0;
           document.getElementById("infos_met"+i).addEventListener("click",function(){
                   clear_todo_met();
                   let nom = document.getElementById("infos_met"+i).textContent;
                   console.log(nom);
                   afficher2[0]=`<h5 style="color:white"> Vins suggérés : </h5><br>`;
                   for(let x=0;x<DATA.length;x++)
                   {
                           if(DATA[x].Nom_met==nom)
                           {
                                   afficher2[j+1]=`<div class="liste">${DATA[x].Nom_vin}</div>`;
                                   j++;
                                   console.log(DATA[x].Nom_vin);
                           }
                   }
                   console.log(afficher2)
                   document.getElementById("info_met").innerHTML =afficher2.join("");
           });
   }

   //fonction qui va supprimer le contenu avant la mise a jour des données demander sur les vins pour les mets 
   function clear_todo_vin()
   {
           while(afficher.length!=0)
           {
                   let i=afficher.length;
                   console.log(i);
                   afficher.splice(i-1);
           }
           document.getElementById("info_vin").innerHTML =afficher.join("");
   }


   //fonction qui va supprimer le contenu avant la mise a jour des données demander sur les mets pour les vins 
   function clear_todo_met()
   {
           while(afficher2.length!=0)
           {
                   let i=afficher2.length;
                   console.log(i);
                   afficher2.splice(i-1);
           }
           document.getElementById("info_met").innerHTML =afficher2.join("");
   }



</script>


<?php $contenu = ob_get_clean(); ?>

<?php require 'templates/' . $_SESSION['currentTemplate']; ?>
