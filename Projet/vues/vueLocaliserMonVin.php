<?php $titre = ""; ?>

<?php ob_start(); ?>

<div class="msgacc">
	<br>
		<h1> Carte des domaines vinicoles/viticoles</h1>
		<br><br>
</div>

<!-- Section attribuée à la recherche sur les listes données par un critère du csv -->
<div class="contenu">
	<div class="row">
		<div class="gauche col-md-3 ">
			<div class="entete_rech">
				<h3> Rechercher par : </h3>
				<select id="tri_recherche" style="width: 100px; height: 30px;">
					<option id="val1" value="val1">Domaine</option>
					<option id="val2" value="val2">Adresse</option>
					<option id="val3" value="val3">Appelation première</option>
				</select>
			</div><br><br>
			<div id="chercher" class="col-12"></div>
		</div>


		<!-- Section attribuée à la map -->
		<div class="droite col-md-9">
			<div id="mapid" class="col-12">
				<div id="mapid2" class="col-12"></div>
			</div>
			<br><br>
			<button id="a"  type="button" class="btn btn-primary">Carte de points</button>
			<button id="b"  type="button" class="btn btn-primary">Carte de densité</button>
			<br><br>
		</div>
		<br>
	</div>
	<br><br>

	<div class="container justify-content-between">
		<div class="rubrique-info">
			<h4> Grâce à la map vous pouvez : </h4><br><br>
			<h7><ul>
				<li><b>Rechercher un vin : </b> Par nom de domaine, adresse ou appellation première du vin.<br> </li><br>
				<li><b> Afficher les domaines présents dans une région : </b> Il vous suffit de cliquer sur la zone souhaité directement sur la carte. <br></li><br>
				<li><b> Filtrer les domaines par caractéristique des vins : </b>. Si vous souhaitez affichez uniquement les vins rouges par exemple ou <br>
						 uniquement les vins liquoreux. </li><br>
				<li><b>Consulter les informations d'un domaine : </b> Nom du domaine, adresse, site du domaine, le vin majoritairement produit et son appellation<br> </li><br>
				<li><b> Découvrir la carte de densité : </b> Dans quelle region se situe la plus grande concentration de domaines vinicoles/viticoles?<br> </li><br>
			</ul>
			</h7>
		</div>
	</div>
</div>
</div>


<script>

	//declaration de variable utile pour le stoker différent état désirer de market sur la map
	var cities = L.layerGroup();

	var Nouvelle_Aquitaine = L.layerGroup();
	var Occitanie = L.layerGroup();
	var Provence_alpes_cotes_dazur= L.layerGroup();
	var Auvergne_rhone_alpes = L.layerGroup();
	var Bourgogne_franche_comte = L.layerGroup();
	var centre_val_de_loire = L.layerGroup();
	var Grand_est = L.layerGroup();

	var Vin_rouge = L.layerGroup();
	var Vin_blanc = L.layerGroup();
	var Vin_rose = L.layerGroup();
	var Vin_mousseux = L.layerGroup();
	var Vin_liquoreux = L.layerGroup();
	var Vin_sec = L.layerGroup();
	var Vin_effervescent = L.layerGroup();
	var Vin_doux = L.layerGroup();


	//création de map sur la <div> avec id='mapid'
	var mymap = L.map('mapid',{worldCopyJump: true,inertia: false}).setView([46.6,1.888], 6);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidHJpdGluIiwiYSI6ImNrNmR3ZHd2dzA4OXoza3FwcWNnMm04cWYifQ.YeKmtUhK3po7wPFPL_24xA', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery  <a href="https://www.mapbox.com/">Mapbox</a>',
			maxZoom: 18,
			minZoom: 1,
			id: 'mapbox/streets-v11',
			accessToken: 'your.mapbox.access.token',
			Layers: [cities]
    }).addTo(mymap);

	//Variable qui est utiliser sur la map en menu déroulement ou l'on pourra clicker pour afficher différent lieux sur la map selon la catégorie choisi
	var overlays = {
		"Toutes les villes": cities,
		"Vin Rouge": Vin_rouge,
		"Vin Blanc": Vin_blanc,
		"Vin_Rosé": Vin_rose,
		"Vin Mousseux": Vin_mousseux,
		"Vin Liquoreux": Vin_liquoreux,
		"Vin Sec": Vin_sec,
		"Vin Effervescent": Vin_effervescent,
		"Vin Doux": Vin_doux
	};

	//Appel a control.layer pour afficher les élément de la variable overlays
	var info2 = L.control.layers({},overlays).addTo(mymap);

	//création de la seconde map qui sera lacarte de densité sur la <div> avec id='mapid2'
	var mymap2 = L.map('mapid2').setView([46.6,1.888], 6);

    var tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidHJpdGluIiwiYSI6ImNrNmR3ZHd2dzA4OXoza3FwcWNnMm04cWYifQ.YeKmtUhK3po7wPFPL_24xA', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery  <a href="https://www.mapbox.com/">Mapbox</a>',
			maxZoom: 18,
            minZoom: 1,
            id: 'mapbox/streets-v11',
            accessToken: 'your.mapbox.access.token'
    }).addTo(mymap2);


	//CODE POUR LE CSV
	var fichierCSV;
    var taillefichierCSV;

	//chargement du fichier csv avec la library Papaparse 
	Papa.parse('vins_du_monde.csv', {
		header: true,
		download: true,
		dynamicTyping: true,
		complete: function(results) {
		console.log(results);
		fichierCSV = results.data;
        taillefichierCSV = results.data.length;
		console.log("taille du fichierCSV : "+taillefichierCSV);

		//fonction qui sera utiliser dans le info.update elle permet de mettre a jour les non de region et nombre de domaine par region dans la zone info 
		function highlightFeature(e) {
			var layer = e.target;
			var nombre = 0 ;
			
			layer.setStyle({
				weight: 5,
				color: '#666',
				dashArray: '',
				fillOpacity: 0.7
			});

			if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
				layer.bringToFront();
			}
			if(layer.feature.properties.nom=="Nouvelle-Aquitaine")
				nombre = Object.keys(Nouvelle_Aquitaine._layers).length;
			if(layer.feature.properties.nom=="Occitanie")
				nombre = Object.keys(Occitanie._layers).length;
			if(layer.feature.properties.nom=="Provence-Alpes-Côte d'Azur")
				nombre = Object.keys(Provence_alpes_cotes_dazur._layers).length;
			if(layer.feature.properties.nom=="Auvergne-Rhône-Alpes")
				nombre = Object.keys(Auvergne_rhone_alpes._layers).length;
			if(layer.feature.properties.nom=="Bourgogne-Franche-Comté")
				nombre = Object.keys(Bourgogne_franche_comte._layers).length;
			if(layer.feature.properties.nom=="Centre-Val de Loire")
				nombre = Object.keys(centre_val_de_loire._layers).length;
			if(layer.feature.properties.nom=="Grand Est")
				nombre = Object.keys(Grand_est._layers).length;
			info.update(layer.feature.properties,nombre);
			console.log(layer.feature.properties.nom);
		}
		
		//fonction de débogage
		function showAlert()
		{
			console.log("Evènement de click détecté");
		}

		//fonction qui rénitialise la zone d'info
		function resetHighlight(e) {
			geojson.resetStyle(e.target);
			info.update();
		}

		//fonction qui zoom sur la region sur laquelle on click et fait aussi la mise a jour des marker a afficher sur la map
		function zoomToFeature(e) {
			mymap.fitBounds(e.target.getBounds());
			e.onclick = showAlert();
			if(lieu!=0)
				clear_popup();
			if(e.target.feature.properties.nom=="Nouvelle-Aquitaine")
			{
				Nouvelle_Aquitaine.addTo(mymap);

				Occitanie.remove(mymap);
				Provence_alpes_cotes_dazur.remove(mymap);
				Auvergne_rhone_alpes.remove(mymap);
				Bourgogne_franche_comte.remove(mymap);
				centre_val_de_loire.remove(mymap);
				Grand_est.remove(mymap);

				init_chercher(e.target.feature.properties.nom);
			}
			else if(e.target.feature.properties.nom=="Occitanie")
			{
				Occitanie.addTo(mymap);

				Nouvelle_Aquitaine.remove(mymap);
				Provence_alpes_cotes_dazur.remove(mymap);
				Auvergne_rhone_alpes.remove(mymap);
				Bourgogne_franche_comte.remove(mymap);
				centre_val_de_loire.remove(mymap);
				Grand_est.remove(mymap);

				init_chercher(e.target.feature.properties.nom);
			}
			else if(e.target.feature.properties.nom=="Provence-Alpes-Côte d'Azur")
			{
				Provence_alpes_cotes_dazur.addTo(mymap);

				Occitanie.remove(mymap);
				Nouvelle_Aquitaine.remove(mymap);
				Auvergne_rhone_alpes.remove(mymap);
				Bourgogne_franche_comte.remove(mymap);
				centre_val_de_loire.remove(mymap);
				Grand_est.remove(mymap);

				init_chercher(e.target.feature.properties.nom);
			}
			else if(e.target.feature.properties.nom=="Auvergne-Rhône-Alpes")
			{
				Auvergne_rhone_alpes.addTo(mymap);

				Occitanie.remove(mymap);
				Provence_alpes_cotes_dazur.remove(mymap);
				Nouvelle_Aquitaine.remove(mymap);
				Bourgogne_franche_comte.remove(mymap);
				centre_val_de_loire.remove(mymap);
				Grand_est.remove(mymap);

				init_chercher(e.target.feature.properties.nom);
			}
			else if(e.target.feature.properties.nom=="Bourgogne-Franche-Comté")
			{
				Bourgogne_franche_comte.addTo(mymap);

				Occitanie.remove(mymap);
				Provence_alpes_cotes_dazur.remove(mymap);
				Auvergne_rhone_alpes.remove(mymap);
				Nouvelle_Aquitaine.remove(mymap);
				centre_val_de_loire.remove(mymap);
				Grand_est.remove(mymap);

				init_chercher(e.target.feature.properties.nom);
			}
			else if(e.target.feature.properties.nom=="Centre-Val de Loire")
			{
				centre_val_de_loire.addTo(mymap);

				Occitanie.remove(mymap);
				Provence_alpes_cotes_dazur.remove(mymap);
				Auvergne_rhone_alpes.remove(mymap);
				Bourgogne_franche_comte.remove(mymap);
				Nouvelle_Aquitaine.remove(mymap);
				Grand_est.remove(mymap);

				init_chercher(e.target.feature.properties.nom);
			}
			else if(e.target.feature.properties.nom=="Grand Est")
			{
				Grand_est.addTo(mymap);

				Occitanie.remove(mymap);
				Provence_alpes_cotes_dazur.remove(mymap);
				Auvergne_rhone_alpes.remove(mymap);
				Bourgogne_franche_comte.remove(mymap);
				centre_val_de_loire.remove(mymap);
				Nouvelle_Aquitaine.remove(mymap);

				init_chercher(e.target.feature.properties.nom);
			}
			else
			{
				Grand_est.remove(mymap);
				Occitanie.remove(mymap);
				Provence_alpes_cotes_dazur.remove(mymap);
				Auvergne_rhone_alpes.remove(mymap);
				Bourgogne_franche_comte.remove(mymap);
				centre_val_de_loire.remove(mymap);
				Nouvelle_Aquitaine.remove(mymap);

				init_chercher(e.target.feature.properties.nom);
			}
		}

		//fonction qui prend en compte les mouvement de la souris et les click réaliser sur la map
		function onEachFeature(feature, layer) {
			layer.on({
				mouseover: highlightFeature,
				mouseout: resetHighlight,
				click: zoomToFeature
			});
		}

		var info = L.control();

		//méthode qui créée une div avec la class info et qui sera utiliser pour la mise à jour des information des region sélectionnées 
		info.onAdd = function (map) {
			this._div = L.DomUtil.create('div', 'info'); // cration de la div avec la class "info"
			this.update();
			return this._div;
		};

		//méthode utiliser pour mettre à jour le controle en fonction des propriété passer en paramètre
		info.update = function (props,nb) {
			this._div.innerHTML = '<h4>Région</h4>' +  (props ?
				'<b>' + props.nom + '</b><br /> '+ nb : 'aucune région sélectionner');

		};

		//ajout de info sur la map
		info.addTo(mymap);

		var geojson;

		//lecture du fichier region.json et ajout sur la map , c'est ce fichier qui permet les zone bleu des régio sur la map 
		$.getJSON('regions.json',function(data){
			console.log(data);
			geojson = L.geoJson(data,{onEachFeature: onEachFeature}).addTo(mymap);
		});
	
		const ville_saisie = Array();
		const ville_saisie2 = Array();

		//fonction qui initialise dans les variable declarer plus haut 'citie,Vin_blanc...' les market qui seront proprement a eux et qui permettront l'affichage selon la catégorie choisi
		function montrer_adr_map(a)
		{
			const heroes = fichierCSV[a];
			let couleur ;

			if(heroes["COULEUR_1"]=="Rouge")
				couleur=heroes["%R"];
			if(heroes["COULEUR_1"]=="Blanc")
				couleur=heroes["%B"];
			if(heroes["COULEUR_1"]=="Rosé")
				couleur=heroes["%Ro"];

			ville_saisie[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(cities);
			ville_saisie[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
			+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur);
			console.log("adresse : "+a);
			console.log(ville_saisie[a]);

			if(heroes["COULEUR_1"]=="Rouge"){
				ville_saisie2[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Vin_rouge);
				ville_saisie2[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie2[a]);
			}

			if(heroes["COULEUR_1"]=="Blanc"){
				ville_saisie2[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Vin_blanc);
				ville_saisie2[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie2[a]);
			}

			if(heroes["COULEUR_1"]=="Rosé"){
				ville_saisie2[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Vin_rose);
				ville_saisie2[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie2[a]);
			}

			if(heroes["Aspect_Type"]=="Mousseux"){
				ville_saisie2[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Vin_mousseux);
				ville_saisie2[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie2[a]);
			}

			if(heroes["Aspect_Type"]=="Liquoreux"){
				ville_saisie2[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Vin_liquoreux);
				ville_saisie2[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie2[a]);
			}

			if(heroes["Aspect_Type"]=="Sec"){
				ville_saisie2[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Vin_sec);
				ville_saisie2[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie2[a]);
			}

			if(heroes["Aspect_Type"]=="Effervescent"){
				ville_saisie2[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Vin_effervescent);
				ville_saisie2[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie2[a]);
			}

			if(heroes["Aspect_Type"]=="Doux"){
				ville_saisie2[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Vin_doux);
				ville_saisie2[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie2[a]);
			}

			if((heroes["region"]==" BORDEAUX RIVE GAUCHE")||(heroes["region"]==" BORDEAUX ET ENTRE DEUX MERS")||(heroes["region"]==" BORDEAUX RIVE DROITE"))
			{
				ville_saisie[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Nouvelle_Aquitaine);
				ville_saisie[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie[a]);
			}
			
			if((heroes["region"]==" SUD-OUEST")||(heroes["region"]==" ROUSSILLON")||(heroes["region"]==" LANGUEDOC"))
			{
				ville_saisie[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Occitanie);
				ville_saisie[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie[a]);
			}
			
			if(heroes["region"]==" PROVENCE")
			{
				ville_saisie[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Provence_alpes_cotes_dazur);
				ville_saisie[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie[a]);
			}
			
			if((heroes["region"]==" VALLEE DU RHONE")||(heroes["region"]==" AUVERGNE"))
			{
				ville_saisie[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Auvergne_rhone_alpes);
				ville_saisie[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie[a]);
			}
			
			if((heroes["region"]==" BEAUJOLAIS")||(heroes["region"]==" BOURGOGNE"))
			{
				ville_saisie[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Bourgogne_franche_comte);
				ville_saisie[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie[a]);
			}
			
			if(heroes["region"]==" LOIRE")
			{
				ville_saisie[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(centre_val_de_loire);
				ville_saisie[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie[a]);
			}
			
			if((heroes["region"]==" ALSACE")||(heroes["region"]==" CHAMPAGNE"))
			{
				ville_saisie[a] = L.marker([heroes["LATITUDE"],heroes["LONGITUDE"]],35).addTo(Grand_est);
				ville_saisie[a].bindPopup("<b> Domaine : "+heroes["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ heroes["Adresse"] +"<br>  Site du Domaine  : <a href="+ heroes["Lien"]+" target=_blank>ICI</a><br>Vin : "
				+heroes["APPELLATION_1"]+"<br>Production Vin : "+heroes["COULEUR_1"]+" à "+couleur+"%");
				console.log("adresse : "+a);
				console.log(ville_saisie[a]);
			}
		}

		var lieu=0;
		//fonction qui va afficher directement les popup du lieu désirer
		function affiche_popup(a)
		{
			let couleur ;

			if(fichierCSV[a]["COULEUR_1"]=="Rouge")
				couleur=fichierCSV[a]["%R"];
			if(fichierCSV[a]["COULEUR_1"]=="Blanc")
				couleur=fichierCSV[a]["%B"];
			console.log(lieu);
			if(lieu!=0)
				clear_popup();
			lieu = L.marker([fichierCSV[a]["LATITUDE"],fichierCSV[a]["LONGITUDE"]],35).addTo(mymap);
			lieu.bindPopup("<b> Domaine : "+fichierCSV[a]["DOMAINE_OU_MAISON"]+"<br>Adresse : "+ fichierCSV[a]["Adresse"] +"<br>  Site du Domaine  : <a href="+ fichierCSV[a]["Lien"]+" target=_blank>ICI</a><br>Vin : "
			+fichierCSV[a]["APPELLATION_1"]+"<br>Production Vin : "+fichierCSV[a]["COULEUR_1"]+" à "+couleur+"%").openPopup();
		}

		//enleve le lieu sur la map
		function clear_popup()
		{
			lieu.remove(mymap);
		}

		//appel a la foncion qui initialise les catégorie de lieu avec les variable
        function get_local_adr(t)
        {
        	montrer_adr_map(t);
		}

		//fonction qui initialiser selon les lieu du fichier csv la catégorie et l'intégrer a une variale propre
		function main()
		{
			for(let j=0 ; j<taillefichierCSV-1; j++)
			{
                 		get_local_adr(j);

			}
		}

		//appel a main()
		main();

		//initialisation du zoom sur la map de dansité
        mymap2.setZoom(6)
		
		//lecture du fichier json sur lequel on applique yne intensité par ligne lors de la lectur du fichier et utilisation de leaflet Heat pour faire la map de chaleur
        $.getJSON('vins_du_monde.json',function(data){
			console.log(data);
            var locations = data.map(function(vin) {
				// the heatmap plugin wants an array of each location
                var location = vin.geometry.coordinates.reverse();
                location.push(0.3);
                return location; // e.g. [50.5, 30.5, 0.2], // lat, lng, intensity
            });

            var heat = L.heatLayer(locations, {
				radius: 35,
				blur: 35,
				maxZoom: 8,
				max: 0.4,

				gradient: {
					0.0: 'yellow',
					0.5: 'orange',
					1.0: 'red'
				}
			});
            console.log(heat);
            mymap2.addLayer(heat);
        });

		//fonction pour enlever les element multiple (doublon,....)
		function getUniqueVal(value, index, self) {
	            return self.indexOf(value) === index;
	    }

		//fonction qui va clear la couleur de l'éllément sélectionner 
		function clearCouleur()
		{
			for(let i=0;i<taillefichierCSV-1;i++)
				if(document.getElementById("infos"+i)!=undefined)
					document.getElementById("infos"+i).style.color='#b8b3b3';
		}

		//fonction qui met en bleu l'élément selectionner 
		function Couleur(i)
		{
			clearCouleur();
			console.log("taille : "+i);
			document.getElementById("infos"+i).style.color='blue';
		}

		var selection = document.getElementById("tri_recherche").value ;

		//fonction qui permet de choisir quelle catégorie afficher entre les domaine,Adresse ou appellation 1 dans le selecteur 
		function Affiche_infos_map(t)
		{
			if (selection==="val1")
				return `<div class="row2" id="infos${t}" >${fichierCSV[t]["DOMAINE_OU_MAISON"]}</div>`;

			if (selection==="val2")
				return `<div class="row2" id="infos${t}" >${fichierCSV[t]["Adresse"]}</div>`;

			if (selection==="val3")
				return `<div class="row2" id="infos${t}" >${fichierCSV[t]["APPELLATION_1"]}</div>`;
		}

		var List=[];
		for(let i=0; i<taillefichierCSV-1;i++)
		{
			List.push(fichierCSV[i]["COULEUR_1"]);
		}
		
		console.log(List);
		var newList=Array();
		newList=List.filter(getUniqueVal);
		console.log(newList);

		//fonction qui va initialiser le selecteur et l'afficher sur le site
		function init_chercher(e){
			selection = document.getElementById("tri_recherche").value ;
			console.log("init");
			let montrer = Array();
			let type;
			let	taille=0
			let info=-1;
			let tab=Array();

			for(let j=0 ; j<taillefichierCSV-1 ; j++)
		    {
				const ville_saisie = Array();
				const heroes = fichierCSV[j];
				if(e==null)
				{
					montrer[j]=Affiche_infos_map(j);
					taille=taillefichierCSV-1;
					if(info==-1)
					{
						info=j;
					}
				}
				if(e=="Nouvelle-Aquitaine")
					if((heroes["region"]==" BORDEAUX RIVE GAUCHE")||(heroes["region"]==" BORDEAUX ET ENTRE DEUX MERS")||(heroes["region"]==" BORDEAUX RIVE DROITE"))
					{
						if(info==-1)
						{
							info=j;
						}
						montrer[j]=Affiche_infos_map(j);
						taille++;
					}

				if(e=="Occitanie")
					if((heroes["region"]==" SUD-OUEST")||(heroes["region"]==" ROUSSILLON")||(heroes["region"]==" LANGUEDOC"))
					{
						if(info==-1)
						{
							info=j;
						}
						montrer[j]=Affiche_infos_map(j);
						taille++;
					}

				if(e=="Provence-Alpes-Côte d'Azur")
					if(heroes["region"]==" PROVENCE")
					{
						if(info==-1)
						{
							info=j;
						}
						montrer[j]=Affiche_infos_map(j);
						taille++;
					}

				if(e=="Auvergne-Rhône-Alpes")
					if((heroes["region"]==" VALLEE DU RHONE")||(heroes["region"]==" AUVERGNE"))
					{
						if(info==-1)
						{
							info=j;
						}
						montrer[j]=Affiche_infos_map(j);
						taille++;
					}

				if(e=="Bourgogne-Franche-Comté")
					if((heroes["region"]==" BEAUJOLAIS")||(heroes["region"]==" BOURGOGNE"))
					{
						if(info==-1)
						{
							info=j;
						}
						montrer[j]=Affiche_infos_map(j);
						taille++;
					}

				if(e=="Centre-Val de Loire")
					if(heroes["region"]==" LOIRE")
					{
						if(info==-1)
						{
							info=j;
						}
						montrer[j]=Affiche_infos_map(j);
						taille++;
					}

				if(e=="Grand Est")
					if((heroes["region"]==" ALSACE")||(heroes["region"]==" CHAMPAGNE"))
					{
						if(info==-1)
						{
							info=j;
						}
						montrer[j]=Affiche_infos_map(j);
						taille++;
					}
			}
			console.log(taille);
			document.getElementById('chercher').innerHTML=montrer.join("");
			if(selection==='val1'){
				type="DOMAINE_OU_MAISON";
			}
			if(selection==='val2'){
				type="Adresse";
			}
			if(selection==='val3'){
				type="APPELLATION_1";
			}

			if(e=="Nouvelle-Aquitaine")
				tab=[" BORDEAUX RIVE GAUCHE"," BORDEAUX ET ENTRE DEUX MERS"," BORDEAUX RIVE DROITE"];
			if(e=="Occitanie")
				tab=[" SUD-OUEST"," ROUSSILLON"," LANGUEDOC"];
			if(e=="Provence-Alpes-Côte d'Azur")
				tab=[" PROVENCE"];
			if(e=="Auvergne-Rhône-Alpes")
				tab=[" VALLEE DU RHONE"," AUVERGNE"];
			if(e=="Bourgogne-Franche-Comté")
				tab=[" BEAUJOLAIS"," BOURGOGNE"];
			if(e=="Centre-Val de Loire")
				tab=[" LOIRE"];
			if(e=="Grand Est")
				tab=[" ALSACE"," CHAMPAGNE"];
			changement(type,tab);
		}

		//fonction qui va zoomer sur le lieu désiré  si l'on click sur un élément du sélecteur '
		function ZoomHorizon(adresse,i)
		{
			mymap.setView([adresse["LATITUDE"],adresse["LONGITUDE"]],14);
			ville_saisie[i].openPopup();
		}

		document.getElementById("tri_recherche")
		.addEventListener("click", () => init_chercher());

		//fonction qui prend en compte les	action faite sur le selecteur 
		function changement(type,e){
			console.log("tab : "+e);
			if(e.length==0)
			{
				for(let i=0;i<taillefichierCSV-1;i++)
				{
					document.getElementById("infos"+i).addEventListener("click", function(){
						affiche_popup(i);
						ZoomHorizon(fichierCSV[i],i);
						Couleur(i);
						console.log(fichierCSV[i]["LATITUDE"]);
						console.log(fichierCSV[i]["LONGITUDE"]);
					});
				}
			}
			else
			{
				for(let i=0;i<taillefichierCSV-1;i++)
				{
					for(let j=0; j<e.length ; j++){
						if(e[j]==fichierCSV[i]["region"]){
							document.getElementById("infos"+i).addEventListener("click", function(){
								ZoomHorizon(fichierCSV[i],i);
								Couleur(i);
								console.log(fichierCSV[i]["LATITUDE"]);
								console.log(fichierCSV[i]["LONGITUDE"]);
							});
						}
					}
				}
			}
		}

		document.getElementById("mapid").style.visibility = 'visible';
		document.getElementById("mapid2").style.visibility = 'hidden';
		
		document.getElementById("a").addEventListener("click", function(){
			document.getElementById("mapid").style.visibility = 'visible';
			document.getElementById("mapid2").style.visibility = 'hidden';
		});

		document.getElementById("b").addEventListener("click", function(){
			document.getElementById("mapid").style.visibility = 'hidden';
			document.getElementById("mapid2").style.visibility = 'visible';
		});

		//appel a init_chercher() en utilisant la valeur null pour rien choisi
		init_chercher(null);

		//Legende carte 
		//couleur a utiliser pour la légend de la map de densité
		function getColor(d) {
			return d > 20  ? '#E31A1C' :
				   d > 10  ? '#FC4E2A' :
				  '#FFEDA0';
		}

		//positionnement de la legend 
		var legend = L.control({position: 'bottomleft'});

		//méthode qui créée une div avec la class info legend et qui sera utiliser pour la légend de la map
		legend.onAdd = function (map) {
			var div = L.DomUtil.create('div', 'info legend'),
			grades = [0, 10, 20],
			labels = [];
			div.innerHTML += '<b> Nombre de domaine <br>'
			//parcour les intervalles de densité et va générer une étiquette avec un carré coloré pour chaque intervalle
			for (var i = 0; i < grades.length; i++) {
				div.innerHTML +=
				'<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
				grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
			}
			return div;
		};

		//ajout de la légend sur la seconde map 
		legend.addTo(mymap2);

		console.log("over");
		console.log(info2._layers);

	}
}); // Fin PapaParse

</script>



<?php $contenu = ob_get_clean(); ?>


<?php require 'templates/' . $_SESSION['currentTemplate']; ?>
