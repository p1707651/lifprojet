<?php $titre = ""; ?>

<?php ob_start(); ?>

<div class=" container" >
        <br><br>
        <div class="msgacc">
                <h1> Accords mets-vins proposés par les utilsateurs </h1>
        </div>
        <br><br><br><br>

</div>

<div id="amvs" class="col-md-12" style="min-height:100%"><b></div>


<script>

//function qui va lire le json et permerttre l'affichage dans la div avec l'id amvs
$.getJSON('Accords_mets_vins_suggestion.json',function(data){
        console.log(data);
        let afficher=Array();
        afficher[0]=`<div class='row col-md-12'>
                        <div style='color:white' class='col-md-1'><h3><u>Vin</u></h3></div>
                        <div style='color:white' class='col-md-1'><h3><u>Met</u></h3></div>
                        <div style='color:white' class='col-md-1'><h3><u>Couleur</u></h3></div>
                        <div style='color:white' class='col-md-2'><h3><u>Categorie</u></h3></div>
                        <div style='color:white' class='col-md-2'><h3><u>Temperature</u></h3></div>
                        <div style='color:white' class='col-md-2'><h3><u>Contributeur</u></h3></div>
                        </div>`;
        for(let i=0;i<data.length;i++)
        {
                afficher[i+1]=`<div class='row col-md-12'>
                                <div style='color:white' class='col-md-1'><h6>${data[i].vin}</h6></div>
                                <div style='color:white' class='col-md-1'><h6>${data[i].met}</h6></div>
                                <div style='color:white' class='col-md-1'><h6>${data[i].couleur}</h6></div>
                                <div style='color:white' class='col-md-2'><h6>${data[i].categorie}</h6></div>
                                <div style='color:white' class='col-md-2'><h6>${data[i].temperature}</h6></div>
                                <div style='color:white' class='col-md-2'><h6>${data[i].contributeur}</h6></div>
                                </div><br>`;
        }
        document.getElementById("amvs").innerHTML=afficher.join("");

});

</script>

<?php $contenu = ob_get_clean(); ?>


<?php require 'templates/' . $_SESSION['currentTemplate']; ?>
