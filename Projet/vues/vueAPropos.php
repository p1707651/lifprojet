<?php $titre = ""; ?>

<?php ob_start(); ?>

<div class="contenu">
        <br>
        <div class="container">
             <div class="msgacc">
                 <h1> A propos de nous. </h1>
             </div>
             <br><br>
        </div>
        <br><br><br>
        <div class="container col-md-12">
            <h4 class="rubrique-info"><b> Qui sommes nous ? Que faisons nous ? </b></h4>
            <br><br>
            <div class="transition">
                <h7 class="rubrique-info"><b><ul>
                    <li> Un groupe composé de trois étudiants : Tri Tin Quach, Talcone Jonathan, Kalambay Océane. </li><br>
                    <li> Actuellement en troisième de licence Informatique, au sein du <a href="https://fst-informatique.univ-lyon1.fr/formation/licences/" target="_blank">
                        Département Informatique</a> du l'Université CLaude Bernard Lyon 1. </li><br>
                    <li> Lors de ce semestre de printemps nous devons réaliser un projet informatique dans le cadre de l'UE.
                        <a href="http://perso.univ-lyon1.fr/fabien.rico/site/projet:2020:pri:presentation" target="_blank">LIFPROJET</a></li><br>
                    <li> Nous développons le projet en autonomie mais avons également un enseignant référant pour nous encadrer et nous conseiller. </li><br>
                    <li> Les travaux se sont déroulés sur environ 3 mois. </li><br>
                <ul>
                </b>
                </h7>
            </div>
        </div>
        <br><br>
        <div class="container col-md-12">
            <h4 class="rubrique-info"><b>Pourquoi ce site ? Les objectifs ? </b></h4>
            <br><br>
            <div class="transition">
                <h7 class="rubrique-info"><b><ul>
                    <li>Mener à bien un projet informatique.</li><br>
                    <li>Explorer un sujet qui nous intéresse. </li><br>
                    <li>Préciser notre orientation future.</li><br>
                    <li>Notre sujet : Valorisation de données d'un commerçant de vin, les détails
                     <a href="http://perso.univ-lyon1.fr/fabien.rico/site/projet:2020:pri:sujet#rc4_valorisation_de_donnees_d_un_commercant_de_vin" target="_blank">
                        ici</a>.</li><br>
                    <li>Les domaines couverts par ce sujet sont : Data visualisation, Site Web et Data Mining</li><br>
                <ul>
                </b>
                </h7>
            </div>
        </div>
</div>



<?php $contenu = ob_get_clean(); ?>


<?php require 'templates/' . $_SESSION['currentTemplate']; ?>
