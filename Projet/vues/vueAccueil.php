<?php $titre = ""; ?>

<?php ob_start(); ?>
        <div class="site_content" id="contenu_site">
            <div class="msgacc">
                    <br>
                    <h1> Bienvenue sur le site Atout-Vins ! </h1>
                    <br>
                    <h4>Vente de vins, champagnes et autres bulles ainsi que des
                    spiritueux auprès des restaurants, cavistes, épiceries fines traiteurs.</h4>
                    <br>
            </div>

            <br><br>

            <div class="container justify-content-between">
                  <img  class="img1 col-md-11" src="image/At1.png" id="image">
            </div>

            <br><br><br>

            <div class="container justify-content-between">
                   <div class="rubrique-info">
                       <h4> Sur ce site, vous retrouverez plusieurs rubriques tels que : </h4><br><br>
                       <h7><ul>
                               <li><b>Localiser mon vin : </b> Grâce à notre carte du monde, parcourez les régions de France à la recherche <br>
                                 de votre vin favori, de vos vignobles préférés ou simplement de l'adresse d'un domaine <br>
                                 près de chez vous. Consultez également notre carte de densité, afin de vous faire une idée sur où se trouve <br>
                                la plus grande concentration de vignobles en France. </li><br><br>
                               <li><b>Cartes mets-vins : </b> Un diner de famille de prévu ? Vous recevez des amis ? <br>
                                       Ou est-ce que vous avez prévu de concocter un bon repas dans le cadre d'Un Diner Presque Parfait ? <br>
                                        Ne cherchez plus cette rubrique est pour vous ! Retrouvez ici des idées d'entrées, plats et/ou desserts
                                        qui accompagneront <br> à merveille le vin que vous dégusterez lors de votre soirée.</li><br><br>
                               <li><b>A propos : </b>Dans cette rubrique, tout simplement, vous trouverez des informations sur nous.<br>
                                        Qui sommes nous ? Que faisons nous ? Pourquoi ce site ? Les objectifs ?</li><br><br>
                           </ul>
                        </h7>
                   </div>
            </div>
        </div>


<!-- Début js -->
<script>

                // Un tableau qui va contenir toutes tes images.
                var images = new Array();
                images.push("image/v13.png");
                images.push("image/At1.png");
                images.push("image/v3.png");
                images.push("image/v11.png");

                var pointeur = 0;

                // fonction pour changer l'image d'accueil tout les 2sec
                function ChangerImage(){
                        document.getElementById("image").src = images[pointeur];

                        if(pointeur < images.length-1){
                                pointeur++;
                        }
                        else{
                                pointeur = 0;
                        }
                        window.setTimeout("ChangerImage()", 2000)
                }

                // Charge la fonction
                window.onload = function(){
                ChangerImage();
                }

</script>

<?php $contenu = ob_get_clean(); ?>

<?php require 'templates/' . $_SESSION['currentTemplate']; ?>
