<?php
session_start();
// index.php fait office de controleur frontal
require('inc/includes.php'); // inclut le fichier avec fonctions (notamment celles du modele)
require('inc/routes.php'); // fichiers de routes

$vue='vueAccueil';

if(isset($_GET['page'])) {
	$nomPage = $_GET['page'];

	if(isset($routes[$nomPage])) {
		$vue = $routes[$nomPage]['vue'];
		}
	}
		include('vues/' . $vue . '.php');
?>