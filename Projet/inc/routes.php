<?php

/*
** Il est possible d'automatiser le routing, notamment en cherchant directement le fichier vue.
** ex, pour page=acceuil : verification de l'existence du fichiers  et vues/vueAccueil.php
** Cela impose un nommage strict des fichiers.
*/

$routes = array(
	'accueil' =>array('vue' => 'vueAccueil'),
	'carte_des_vins' =>array('vue' => 'vueCarteDesVins'),
	'localiser_mon_vin' =>array('vue' => 'vueLocaliserMonVin'),
	'a_propos' =>array('vue' => 'vueAPropos'),
	'accords_mets_vins' =>array('vue' => 'vueAccords')
);

?>
