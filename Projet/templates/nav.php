<!--Menu -->
<div class="menu">
        <nav class="site-nav sticky-top py-2">
                <div class="container d-flex flex-column flex-md-row justify-content-between">
                        <a class="nav col-sm-1" href="index.php?page=accueil"><i class="fa fa-glass fa-3x" style="color:#a4155a"></i></a>

                        <a class="nav py-2 col-sm-2" href="index.php?page=accueil"> Accueil </a>
                        <a class="nav py-2 col-sm-2" href="index.php?page=carte_des_vins"> Carte mets-vins </a>
                        <a class="nav py-2 col-sm-2" href="index.php?page=localiser_mon_vin"> Localiser mon vin </a>
                        <a class="nav py-2 col-sm-2" href="index.php?page=a_propos"> A propos </a>

                        <form class="nav py-2 col-sm-3" name="search" onSubmit="return findInPage(this.motcle.value);">
                        <input class="nav py-2 col-sm-9" name="motcle" type="text" size=30 onFocus="nbSearch=0; if (this.value=='Chercher sur la page') {this.value=''}" value="Chercher sur la page">
                        <button type="submit" value="OK" class="btn btn-xs btn-light py-1" id="search-btn">
                                <i class="fa fa-search fa-1x" aria-hidden="true"></i>
                        </button>

                        </form>
                </div>

        </nav>
</div>


<script>

var nbSearch=0;
//fonction de recherche sur la page 
function findInPage(str) {
    var txt, i, found;
    if (str=="") return false;

    if ((document.layers)||(window.sidebar)) {
        if (!window.find(str)) {
            alert("Fin de page atteinte.\n"+'"'+str+'" trouvé '+nbSearch+" fois.");
            while(window.find(str, false, true)) {nbSearch++;}
        }
        else
            nbSearch++;
        if (nbSearch == 0)
            alert('"'+str+'" est introuvable');
    }

    if (document.all) {
        txt = window.document.body.createTextRange();
        for (i = 0; i <= nbSearch && (found = txt.findText(str)) != false; i++) {
            txt.moveStart("character", 1);
            txt.moveEnd("textedit");
        }
        if (found) {
            txt.moveStart("character", -1);
            txt.findText(str);
            txt.select();
            txt.scrollIntoView();
            nbSearch++;
        } else {
            if (nbSearch > 0) {
                alert("Fin de page atteinte.\n"+'"'+str+'" trouvé '+nbSearch+" fois.");
                nbSearch = 0;
                findInPage(str);
            } else {
                alert('"'+str+'" est introuvable');
            }
        }
    }
    return false;
}
</script>
