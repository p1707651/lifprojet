<!-- Pied de page -->
<div>
        <footer class="page-footer">
                <!--  les auteurs -->
                <div class="container d-flex flex-column flex-md-row justify-content-between">


                	    <span class="footer py-2 col-sm-3" style="font-weight:bold" > KALAMBAY Océane </span>
                        <span class="footer py-2 col-sm-3" style="font-weight:bold" > TALCONE Jonathan </span>
                        <span class="footer py-2 col-sm-2" style="font-weight:bold" > QUACH Tri Tin </span>

                        <!-- le lien  vers le site de l'université -->
                        <span class="footer py-2 col-sm-3"><a href="https://www.univ-lyon1.fr/" target="_blank" >Université Lyon 1 </a></span>
                        <!-- les droits -->
                    	<span class="footer py-2 col-sm-2"><a href="https://creativecommons.org/licenses/" target="_blank"><img src="image/by-nc-sa-eu.png" alt="Licence CC BY-NC-SA"/></a></span>
                </div>
        </footer>
</div>
