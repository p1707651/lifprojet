# **Projet Valorisation de données d'un commerçant de vin**

## Auteurs

Fait par: Talcone Jonathan 11707651  Kalambay Océane 116096310  Quach Tri Tin 11608570 

[](url)https://forge.univ-lyon1.fr/p1707651/lifprojet/-/graphs/master

## Objectif

Une commercante en Vin a une base de données (Excel) avec les informations de ses clients et des crus qu'elle représente. 
A partir de ces données, vous créerez un site web permettant a des clients (restaurateurs, particuliers, etc.) de visualiser sur une carte les vins représentés, 
de pouvoir chercher par information (cépage, couleur…), d'accéder aux sites web des domaines, etc. 
Vous récupèrerez également des données accessibles en opendata pour enrichir le site (par exemple, proposition d'accord mets-vins).

## Le Rendu

Le rendu est sous forme de site web accessible au lien suivant : [](url)http://lif.sci-web.net/~qkt2020/Atouts-vin/

Ou encore faire un git clone et ajouter le dossier sur un serveur local comme WampServer (sous windows) [](url)https://sourceforge.net/projects/wampserver/ , 
Apache2 (sous linux) [](url) https://doc.ubuntu-fr.org/apache2 ...
Le site s'ouvrira directement sur index.php. 


![acceuil](Projet/data/accueil.png)

### Language 

Les languges que nous avons utilisé pour développer ce projet sont HTML et CSS pour le front-end , le javascript qui nous a permis le codage du back-end et le PHP que l'on a utilisé
afin de mettre notre en site en MVC. 


### Framework et bibliothèques

Pour mener ce projet a bien nous avons utilisez plusieurs Framework tels que Boostrap qui sert à améliorer le rendu d'une page web 
avec du CSS et de l'HTML, Font-awesome qui est en lien avec Boostrap ce framework permet d'inclure des icones sur une page web, nous avons
aussi utilisé la bibliothèque Papaparse qui permet de lire et d'utiliser plus facilement des fichier csv car malgré le 'csv' malformé Papaparse arrive à le corriger
et le lire correctement, JQuery que l'on utilise afin de charger et manipuler des fichiers 'json' sur le site, grâce a la librairie Ajax et en faisant le lien avec PHP nous avons
pu créer une requête qui permet d'ajouter des entrées dans un fichier et leafletHeat qui est une bibliothèque en lien avec l'API leaflet.

### API

Nous avons pu exploiter des API gratuites pour ce projet tel que L'API du gouv permettant de récupérer des adresses. Elle s'utilise en éxécutant dans la barre de recherche :  
`https://api-adresse.data.gouv.fr/search/?q=+Adresse_shouaité`  
où `adresse_shouaité` et l'adresse du lieu que l'on recherche, example: `https://api-adresse.data.gouv.fr/search/?q=viriville`.  
Pour faire une carte afin de répertorier les domaines nous avons utilisé l'API de leaflet que l'on charge directement sur notre page avec :  
`<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>`  
`<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="crossorigin=""></script>`  
ce qui permet de charger le css et le script javascript nécessaires au chargement et à l'utilisation d'une carte sur le site.  


![carte](Projet/data/carte.png)  
*Aperçu visuel des API utilisées (Leaflet, API du gouv)*

### Arborescence

Le dossier *Projet* contient les sous-dossier suivant :

*  *PapaParse-5.0.2*  qui sera nécessaire pour la transformation du csv en json sur le site (le chargement de Papaparse ce fait en local dans le fichier template.php)
*  *boostrap*         nécessaire au style et au modèle responsive de la page (charger en local dans le template.php)
*  *css*              style.css  utiliser pour le style du site sur les "id", "class" et balises désirées ( fichier chargé en local dans le template.php)
*  *font-awesome*     librairie d'icones 
*  *image*            dans ce dossier se trouve les images utilisées dans le site pour les mets mais aussi des images pour la page d'accueil et es autres pages.
*  *inc*              dans ce dossier nous avons les fichiers includes.php qui utilise des variable constante pour le site comme le nom du site... et routes.php qui mettra en lien les différentes pages du site
*  *templates*        dans ce dossier nous avons les fichiers header.php qui affiche une image sur l'en tête du site, le nav.php qui est le menu composé de plusieurs onglets, footer.php qui est le pied de page et template.php qui fait le chargement des fichiers locaux et extérieur nécessaire pour le fonctionnement du site et qui inclut le nav.php,header.php et footer.php
*  *vues*             dans ce dossier il y a 5 fichiers vueAccueil.php, vueAccords.php, vueAPropos.php, vueCarteDesVins.php et vueLocaliserMonVin.php chaque fichier est écrit en html et est constitué de 'script' (code JS) où nous trouverons des fonctions propres à chaque vue du site   
*  *index.php*        c'est ce fichier qui fait office de controleur "frontal" c'est lui qui avec les liens contenu dans routes.php peut trouver ou non le chemin de la page désiré sur le site    

Le reste des fichiers sont des fichiers de données que nous avons exploités pour le contenu du site .  

### Problème

La fonction de la barre de recherche est seulement implémenter pour Firefox elle n'est pas fonctionnelle pour google chrome .
